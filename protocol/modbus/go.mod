module iot-modbus

go 1.22

require (
	github.com/goburrow/modbus v0.1.0
	github.com/google/uuid v1.6.0
	github.com/rabbitmq/amqp091-go v1.10.0
	github.com/redis/go-redis/v9 v9.6.0
	github.com/tbrandon/mbserver v0.0.0-20231208015628-36eb59221ac2
	go.uber.org/zap v1.27.0
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
)
